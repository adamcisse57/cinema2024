import { createRouter, createWebHistory } from 'vue-router';
import Accueil from '@/views/Accueil.vue';
import RechercheFilms from '@/views/RechercheFilms.vue';
import ActorDetails from '@/views/ActorDetails.vue'; // Correction de l'importation
import Details from '@/views/Details.vue';

const routes = [
    {
        path: '/',
        name: 'Accueil',
        component: Accueil
    },
    {
        path: '/recherche',
        name: 'Recherche',
        component: RechercheFilms
    },
    {
        path: "/details",
        name: "details",
        component: Details,
    },
    {
        path: '/actor/:id',
        component: ActorDetails,
        name: 'actor-details'
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;
